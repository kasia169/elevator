import com.google.gson.Gson;
import elevator.Elevator;
import person.Person;
import untils.Address;
import untils.City;
import weight.Weight;
import weight.WeightMetrics;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 * Created by RENT on 2017-08-21.
 */
public class Main {

    public static void main(String[] args) {
        Person person = new Person();
        person.setmFirstName("Kasia");
        person.setmName("Adamczyk");
        person.setmAdress(new Address().setCity(City.KATOWICE).setStreetName("Kolorowa").setBlockNo(4).setApartamentNo(5).setPostalCode("Katowice"));
        person.setmWeight(new Weight(70, WeightMetrics.KG));

        System.out.println(person);

    }

}


