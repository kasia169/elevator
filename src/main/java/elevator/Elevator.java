package elevator;

import elevator.Direction;
import floor.Floor;
import weight.Weight;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-08-21.
 */
public class Elevator {

    private List <Floor> mAvailableFloors;
    private List <Floor> mRequestFloors = new ArrayList<Floor>();
    private Floor mCurrentFloor;
    private boolean mServiceBreak = false;
    private Weight mMaxLoad;
    Direction mDirection = Direction.DOWN;


    public List<Floor> getmAvailableFloors() {
        return mAvailableFloors;
    }

    public void setmAvailableFloors(List<Floor> mAvailableFloors) {
        this.mAvailableFloors = mAvailableFloors;
    }

    public List<Floor> getmRequestFloors() {
        return mRequestFloors;
    }

    public void setmRequestFloors(List<Floor> mRequestFloors) {
        this.mRequestFloors = mRequestFloors;
    }

    public Floor getmCurrentFloor() {
        return mCurrentFloor;
    }

    public void setmCurrentFloor(Floor mCurrentFloor) {
        this.mCurrentFloor = mCurrentFloor;
    }

    public boolean ismServiceBreak() {
        return mServiceBreak;
    }

    public void setmServiceBreak(boolean mServiceBreak) {
        this.mServiceBreak = mServiceBreak;
    }

    public Weight getmMaxLoad() {
        return mMaxLoad;
    }

    public void setmMaxLoad(Weight mMaxLoad) {
        this.mMaxLoad = mMaxLoad;
    }

    public Direction getmDirection() {
        return mDirection;
    }

    public void setmDirection(Direction mDirection) {
        this.mDirection = mDirection;
    }
}
