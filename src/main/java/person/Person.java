package person;

import elevator.Elevator;
import untils.Address;
import weight.Weight;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by RENT on 2017-08-21.
 */
public class Person  {

    String mFirstName;
    String mName;

    Address mAdress;
    Weight mWeight;

    public Person() {
        this.mFirstName = mFirstName;
        this.mName = mName;
        this.mAdress = mAdress;
        this.mWeight = mWeight;
    }

    public String getmFirstName() {
        return mFirstName;
    }

    public void setmFirstName(String mFirstName) {
        this.mFirstName = mFirstName;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public Address getmAdress() {
        return mAdress;
    }

    public void setmAdress(Address mAdress) {
        this.mAdress = mAdress;
    }

    public Weight getmWeight() {
        return mWeight;
    }

    public void setmWeight(Weight mWeight) {
        this.mWeight = mWeight;
    }
    @Override
    public String toString(){
        return  "dane osoby: " + this.getmFirstName() + " " + this.getmName() +  " " + this.getmWeight() + " "+ this.getmAdress();
    }
}
