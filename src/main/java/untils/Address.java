package untils;

/**
 * Created by RENT on 2017-08-22.
 */
public class Address {

    City city;
    String streetName;
    int blockNo;
    int apartamentNo;
    String postalCode;

    public Address() {
    }

    public City getCity() {
        return city;
    }

    public Address setCity(City city) {
        this.city = city;
        return this;

    }

    public String getStreetName() {
        return streetName;
    }

    public Address setStreetName(String streetName) {
        this.streetName = streetName;
        return this;
    }

    public int getBlockNo() {
        return blockNo;
    }

    public Address setBlockNo(int blockNo) {
        this.blockNo = blockNo;
        return this;
    }

    public int getApartamentNo() {
        return apartamentNo;
    }

    public Address setApartamentNo(int apartamentNo) {
        this.apartamentNo = apartamentNo;
        return this;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public Address setPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    @Override
    public String toString() {
        return getStreetName() + getCity() + getApartamentNo() + getBlockNo() +getPostalCode();
}
}
