package untils;

/**
 * Created by RENT on 2017-08-22.
 */
public enum City {
    LUBLIN("Lublin"), WARSZAWA("Warszawa"), GDAŃSK("Gdańsk"), ŁÓDŹ("Łódź"), STALOWA_WOLA("Stalowa Wola") , KATOWICE("Katowice");

    String cityName;

    City(String cityName) {
        this.cityName = cityName;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    @Override
    public String toString() {
        return getCityName();
    }
}
