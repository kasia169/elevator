package weight;

/**
 * Created by RENT on 2017-08-21.
 */
public class Weight {
    //dopisac testy jednostkowe

    int amount;
    WeightMetrics weightMetrics;

    public Weight(int amount, WeightMetrics weightMetrics) {
        this.amount = amount;
        this.weightMetrics = weightMetrics;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public WeightMetrics getWeightMetrics() {
        return weightMetrics;
    }

    public void setWeightMetrics(WeightMetrics weightMetrics) {
        this.weightMetrics = weightMetrics;
    }

    public float getWeight(WeightMetrics weightMetrics) {
        if (weightMetrics == this.getWeightMetrics()) {
            return this.getAmount();
        } else if (this.getWeightMetrics() ==  weightMetrics.LB)
            return this.getAmount() / 0.45f;
        else {
            return this.getAmount() * 0.45f;
        }
    }

    @Override
    public String toString(){
        return getAmount() + " " + getWeightMetrics();
    }
}
